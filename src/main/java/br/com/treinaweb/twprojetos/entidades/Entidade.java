package br.com.treinaweb.twprojetos.entidades;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.hateoas.RepresentationModel;

import lombok.Data;

@MappedSuperclass
@Data
public abstract class Entidade extends RepresentationModel<Entidade> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

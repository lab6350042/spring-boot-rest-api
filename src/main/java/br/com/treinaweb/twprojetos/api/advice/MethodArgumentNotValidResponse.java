package br.com.treinaweb.twprojetos.api.advice;

import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MethodArgumentNotValidResponse extends EntityNotFoundResponse {

  private List<FieldError> errors;

  public MethodArgumentNotValidResponse(Integer error, String status, LocalDateTime timestamp, String message,
      List<FieldError> errors) {
    super(error, status, timestamp, message);
    this.errors = errors;
  }
}
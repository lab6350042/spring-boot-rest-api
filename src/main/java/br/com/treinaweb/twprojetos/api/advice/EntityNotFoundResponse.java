package br.com.treinaweb.twprojetos.api.advice;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EntityNotFoundResponse {

  private Integer error;

  private String status;

  private LocalDateTime timestamp;

  private String message;
}

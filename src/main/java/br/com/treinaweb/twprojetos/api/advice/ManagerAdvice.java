package br.com.treinaweb.twprojetos.api.advice;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice(annotations = RestController.class)
public class ManagerAdvice extends ResponseEntityExceptionHandler {

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<EntityNotFoundResponse> handleEntityNotFoundException(EntityNotFoundException exception) {
    HttpStatus httpStatus = HttpStatus.NOT_FOUND;

    EntityNotFoundResponse response = new EntityNotFoundResponse(
      httpStatus.value(),
      httpStatus.getReasonPhrase(),
      LocalDateTime.now(),
      exception.getLocalizedMessage()
    );

    return new ResponseEntity<>(response, httpStatus);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers,
      HttpStatus status, WebRequest request) {

        List<FieldError> errors = new ArrayList<>();
        exception.getBindingResult().getFieldErrors().forEach(error -> {
            FieldError fieldError = new FieldError(
                    error.getField(),
                    error.getDefaultMessage()
            );

            errors.add(fieldError);
        });

        MethodArgumentNotValidResponse response = new MethodArgumentNotValidResponse(
             status.value(),
             status.getReasonPhrase(),
             LocalDateTime.now(),
             "The request contains validation errors",
             errors
        );

        return new ResponseEntity<>(response, status);
  }
}

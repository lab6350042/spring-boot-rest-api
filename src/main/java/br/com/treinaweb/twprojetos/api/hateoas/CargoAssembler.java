package br.com.treinaweb.twprojetos.api.hateoas;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.SimpleRepresentationModelAssembler;
import org.springframework.stereotype.Component;

import br.com.treinaweb.twprojetos.api.controller.CargoController;
import br.com.treinaweb.twprojetos.entidades.Cargo;

@Component
public class CargoAssembler implements SimpleRepresentationModelAssembler<Cargo> {

  @Override
  public void addLinks(EntityModel<Cargo> resource) {
    Long id = resource.getContent().getId();

    Link selfLink = linkTo(methodOn(CargoController.class).findById(id))
        .withSelfRel()
        .withType("GET");

    Link editLink = linkTo(methodOn(CargoController.class).updateCargo(null, id))
        .withSelfRel()
        .withType("PUT");

    Link deleteLink = linkTo(methodOn(CargoController.class).delteById(id))
        .withSelfRel()
        .withType("DELETE");

    resource.add(selfLink, editLink, deleteLink);
  }

  @Override
  public void addLinks(CollectionModel<EntityModel<Cargo>> resources) {
    Link selfLink = linkTo(methodOn(CargoController.class).findAll(null))
    .withSelfRel()
    .withType("GET");

    Link registerLink = linkTo(methodOn(CargoController.class).createCargo(null))
    .withSelfRel()
    .withType("POST");

    resources.add(selfLink, registerLink);
  }

}

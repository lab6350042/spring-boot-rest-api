package br.com.treinaweb.twprojetos.api.hateoas;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.SimpleRepresentationModelAssembler;
import org.springframework.stereotype.Component;

import br.com.treinaweb.twprojetos.api.controller.ClienteController;
import br.com.treinaweb.twprojetos.api.controller.FuncionarioController;
import br.com.treinaweb.twprojetos.api.controller.ProjetoController;
import br.com.treinaweb.twprojetos.entidades.Projeto;

@Component
public class ProjetoAssembler implements SimpleRepresentationModelAssembler<Projeto> {

  @Override
  public void addLinks(EntityModel<Projeto> resource) {
    Long id = resource.getContent().getId();
    Long liderId = resource.getContent().getLider().getId();
    Long clienteId = resource.getContent().getCliente().getId();

    Link selfLink = linkTo(methodOn(ProjetoController.class).findById(id))
        .withSelfRel()
        .withType("GET");

    Link editLink = linkTo(methodOn(ProjetoController.class).updateProjeto(null, id))
        .withSelfRel()
        .withType("PUT");

    Link deleteLink = linkTo(methodOn(ProjetoController.class).deleteById(id))
        .withSelfRel()
        .withType("DELETE");

    Link liderLink = linkTo(methodOn(FuncionarioController.class).findById(liderId))
        .withRel("lider")
        .withType("GET");

    Link clienteLink = linkTo(methodOn(ClienteController.class).findById(clienteId))
        .withRel("cliente")
        .withType("GET");

    Link equipeLink = linkTo(methodOn(ProjetoController.class).findEquipe(id))
        .withRel("equipe")
        .withType("GET");

    Link updateEquipe = linkTo(methodOn(ProjetoController.class).updateEquipe(id, null))
        .withRel("equipe")
        .withType("PATCH");

    resource.add(selfLink, editLink, deleteLink, liderLink, clienteLink, equipeLink, updateEquipe);
  }

  @Override
  public void addLinks(CollectionModel<EntityModel<Projeto>> resources) {
    Link selfLink = linkTo(methodOn(ProjetoController.class).findAll(null))
        .withSelfRel()
        .withType("GET");

    resources.add(selfLink);
  }

}

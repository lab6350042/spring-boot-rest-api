package br.com.treinaweb.twprojetos.api.advice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FieldError {

  private String field;

  private String message;
}

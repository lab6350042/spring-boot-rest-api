package br.com.treinaweb.twprojetos.api.docs;

import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;

import br.com.treinaweb.twprojetos.api.Anotations.ApiPageable;
import br.com.treinaweb.twprojetos.api.advice.EntityNotFoundResponse;
import br.com.treinaweb.twprojetos.api.dto.CargoDto;
import br.com.treinaweb.twprojetos.entidades.Cargo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = "Cargos", description = "Cargo controller")
public interface CargoControllerDoc {

  @ApiOperation(value = "Listar todos os cargos")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Listagem dos cargos realizada com sucesso.")
  })
  @ApiPageable
  CollectionModel<EntityModel<Cargo>> findAll(@ApiIgnore Pageable pageable);

  @ApiOperation(value = "Buscar cargo por id")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Cargo encontrado com sucesso."),
      @ApiResponse(code = 404, message = "Cargo não encontrado.", response = EntityNotFoundResponse.class)
  })
  EntityModel<Cargo> findById(Long id);

  @ApiOperation(value = "Cadastrar cargo")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Cargo cadastrado com sucesso."),
      @ApiResponse(code = 400, message = "Houveram erros de validação.", response = MethodArgumentNotValidException.class)
  })
  EntityModel<Cargo> createCargo(CargoDto cargoDto);

  @ApiOperation(value = "Atualizar cargo")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Cargo atualizado com sucesso."),
      @ApiResponse(code = 400, message = "Houveram erros de validação.", response = MethodArgumentNotValidException.class),
      @ApiResponse(code = 404, message = "Cargo não encontrado.", response = EntityNotFoundResponse.class)
  })
  EntityModel<Cargo> updateCargo(CargoDto cargoDto, Long id);

  @ApiOperation(value = "Excluir cargo por id")
  @ApiResponses(value = {
      @ApiResponse(code = 204, message = "Cargo excluido com sucesso."),
      @ApiResponse(code = 404, message = "Cargo não encontrado.", response = EntityNotFoundResponse.class)
  })
  ResponseEntity<?> delteById(Long id);
}
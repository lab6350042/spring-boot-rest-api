package br.com.treinaweb.twprojetos.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinaweb.twprojetos.api.docs.CargoControllerDoc;
import br.com.treinaweb.twprojetos.api.dto.CargoDto;
import br.com.treinaweb.twprojetos.api.hateoas.CargoAssembler;
import br.com.treinaweb.twprojetos.entidades.Cargo;
import br.com.treinaweb.twprojetos.servicos.CargoServico;

@RestController
@RequestMapping("/api/v1/cargos")
public class CargoController implements CargoControllerDoc {

  @Autowired
  private CargoServico cargoServico;

  @Autowired
  private CargoAssembler cargoAssembler;

  @Autowired
  private PagedResourcesAssembler<Cargo> pagedResourcesAssembler;

  @GetMapping
  public CollectionModel<EntityModel<Cargo>> findAll(Pageable pageable) {
     Page<Cargo> cargos = cargoServico.buscarTodos(pageable);

     return pagedResourcesAssembler.toModel(cargos, cargoAssembler);
  }

  @GetMapping("/{id}")
  public EntityModel<Cargo> findById(@PathVariable Long id) {
    Cargo cargo = cargoServico.buscarPorId(id);

    return cargoAssembler.toModel(cargo);
  }

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public EntityModel<Cargo> createCargo(@RequestBody @Valid CargoDto cargoDto) {
    Cargo cargo = cargoServico.cadastrar(cargoDto);

    return cargoAssembler.toModel(cargo);
  }

  @PutMapping("/{id}")
  public EntityModel<Cargo> updateCargo(@RequestBody @Valid CargoDto cargoDto, @PathVariable Long id) {
    Cargo cargo = cargoServico.atualizar(cargoDto, id);

    return cargoAssembler.toModel(cargo);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> delteById(@PathVariable Long id) {
    cargoServico.excluirPorId(id);

    return ResponseEntity.noContent().build();
  }

}

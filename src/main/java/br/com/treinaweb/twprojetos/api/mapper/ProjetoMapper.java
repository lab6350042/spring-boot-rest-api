package br.com.treinaweb.twprojetos.api.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.treinaweb.twprojetos.api.dto.ProjetoDto;
import br.com.treinaweb.twprojetos.entidades.Projeto;
import br.com.treinaweb.twprojetos.servicos.ClienteServico;
import br.com.treinaweb.twprojetos.servicos.FuncionarioServico;

@Component
public class ProjetoMapper {

  @Autowired
  private FuncionarioServico funcionarioServico;

  @Autowired
  private ClienteServico clienteServico;

  public Projeto fromDto(ProjetoDto projetoDto) {
    Projeto projeto = new Projeto();

    projeto.setNome(projetoDto.getNome());
    projeto.setDescricao(projetoDto.getDescricao());
    projeto.setDataInicio(projetoDto.getDataInicio());
    projeto.setDataFim(projetoDto.getDataFim());
    projeto.setOrcamento(projetoDto.getOrcamento());
    projeto.setGastos(projetoDto.getGastos());

    projeto.setLider(funcionarioServico.buscarPorId(projetoDto.getLiderId()));
    projeto.setCliente(clienteServico.buscarPorId(projetoDto.getClienteId()));

    return projeto;
  }

}

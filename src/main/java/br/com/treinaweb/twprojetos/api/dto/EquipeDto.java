package br.com.treinaweb.twprojetos.api.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class EquipeDto {
  
  @NotNull
  @NotEmpty
  private List<Long> equipe;
}

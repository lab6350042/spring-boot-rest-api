package br.com.treinaweb.twprojetos.api.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.Data;

@Data
public class ProjetoDto {

    @NotNull
    @Size(min = 3, max = 255)
    private String nome;

    private String descricao;

    @NotNull
    @PastOrPresent
    @DateTimeFormat(iso = ISO.DATE)
    private LocalDate dataInicio;

    @PastOrPresent
    @DateTimeFormat(iso = ISO.DATE)
    private LocalDate dataFim;

    @NotNull
    @Positive
    private Long clienteId;

    @NotNull
    @Positive
    private Long liderId;

    @NotNull
    private BigDecimal orcamento;

    @NotNull
    private BigDecimal gastos;
}

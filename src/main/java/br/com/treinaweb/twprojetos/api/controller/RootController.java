package br.com.treinaweb.twprojetos.api.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinaweb.twprojetos.api.hateoas.RootModel;

@RestController
@RequestMapping("/api/v1")
public class RootController {

  @GetMapping
  public RootModel root() {
    RootModel rootModel = new RootModel();

    Link cargosLink = linkTo(methodOn(CargoController.class).findAll(null))
    .withRel("cargos")
    .withType("GET");

    rootModel.add(cargosLink);

    return rootModel;
  }
}
package br.com.treinaweb.twprojetos.api.hateoas;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.SimpleRepresentationModelAssembler;
import org.springframework.stereotype.Component;

import br.com.treinaweb.twprojetos.api.controller.CargoController;
import br.com.treinaweb.twprojetos.api.controller.FuncionarioController;
import br.com.treinaweb.twprojetos.entidades.Funcionario;

@Component
public class FuncionarioAssembler implements SimpleRepresentationModelAssembler<Funcionario> {

  @Override
  public void addLinks(EntityModel<Funcionario> resource) {
    Long cargoId = resource.getContent().getCargo().getId();
    Long id = resource.getContent().getId();

    Link cargoLink = linkTo(methodOn(CargoController.class).findById(cargoId))
        .withRel("cargo")
        .withType("GET");

    Link selfLink = linkTo(methodOn(FuncionarioController.class).findById(id))
        .withSelfRel()
        .withType("GET");

    resource.add(cargoLink, selfLink);
  }

  @Override
  public void addLinks(CollectionModel<EntityModel<Funcionario>> resources) {
    Link selfLink = linkTo(methodOn(FuncionarioController.class).findAll(null))
        .withSelfRel()
        .withType("GET");

    resources.add(selfLink);
  }
}

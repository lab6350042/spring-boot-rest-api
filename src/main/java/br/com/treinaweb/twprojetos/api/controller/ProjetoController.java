package br.com.treinaweb.twprojetos.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinaweb.twprojetos.api.dto.EquipeDto;
import br.com.treinaweb.twprojetos.api.dto.ProjetoDto;
import br.com.treinaweb.twprojetos.api.hateoas.FuncionarioAssembler;
import br.com.treinaweb.twprojetos.api.hateoas.ProjetoAssembler;
import br.com.treinaweb.twprojetos.entidades.Funcionario;
import br.com.treinaweb.twprojetos.entidades.Projeto;
import br.com.treinaweb.twprojetos.servicos.ProjetoServico;

@RestController
@RequestMapping("/api/v1/projetos")
public class ProjetoController {

  @Autowired
  private ProjetoServico projetoServico;

  @Autowired
  private ProjetoAssembler projetoAssembler;

  @Autowired
  private PagedResourcesAssembler<Projeto> pagedResourcesAssembler;

  @Autowired
  private FuncionarioAssembler funcionarioAssembler;

  @GetMapping
  public CollectionModel<EntityModel<Projeto>> findAll(Pageable pageable) {
    Page<Projeto> projetos = projetoServico.buscarTodos(pageable);

    return pagedResourcesAssembler.toModel(projetos, projetoAssembler);
  }

  @GetMapping("/{id}")
  public EntityModel<Projeto> findById(@PathVariable Long id) {
    Projeto projeto = projetoServico.buscarPorId(id);

    return projetoAssembler.toModel(projeto);
  }

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public EntityModel<Projeto> createProject(@RequestBody @Valid ProjetoDto projetoDto) {
    Projeto projeto = projetoServico.cadastrar(projetoDto);

    return projetoAssembler.toModel(projeto);
  }

  @PutMapping("/{id}")
  public EntityModel<Projeto> updateProjeto(@RequestBody @Valid ProjetoDto projetoDto, @PathVariable Long id) {
    Projeto projeto = projetoServico.atualizar(projetoDto, id);

    return projetoAssembler.toModel(projeto);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public ResponseEntity<?> deleteById(@PathVariable Long id) {
    projetoServico.excluirPorId(id);

    return ResponseEntity.noContent().build();
  }

  @GetMapping("/{id}/equipe")
  public CollectionModel<EntityModel<Funcionario>> findEquipe(@PathVariable Long id) {
    List<Funcionario> equipe = projetoServico.buscarPorId(id).getEquipe();

    return funcionarioAssembler.toCollectionModel(equipe);
  }

  @PatchMapping("/{id}/equipe")
  public CollectionModel<EntityModel<Funcionario>> updateEquipe(@PathVariable Long id,
      @RequestBody @Valid EquipeDto equipeDto) {
        List<Funcionario> equipe =  projetoServico.atualizarEquipe(equipeDto, id);

        return funcionarioAssembler.toCollectionModel(equipe);
  }
}

package br.com.treinaweb.twprojetos.servicos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.treinaweb.twprojetos.api.dto.CargoDto;
import br.com.treinaweb.twprojetos.api.mapper.CargoMapper;
import br.com.treinaweb.twprojetos.entidades.Cargo;
import br.com.treinaweb.twprojetos.excecoes.CargoNaoEncontradoException;
import br.com.treinaweb.twprojetos.excecoes.CargoPossuiFuncionariosException;
import br.com.treinaweb.twprojetos.repositorios.CargoRepositorio;
import br.com.treinaweb.twprojetos.repositorios.FuncionarioRepositorio;

@Service
public class CargoServico {

    @Autowired
    private CargoRepositorio cargoRepositorio;

    @Autowired
    private FuncionarioRepositorio funcionarioRepositorio;

    @Autowired
    private CargoMapper cargoMapper;

    public List<Cargo> buscarTodos() {
        return cargoRepositorio.findAll();
    }

    public Page<Cargo> buscarTodos(Pageable pageable) {
        return cargoRepositorio.findAll(pageable);
    }

    public Cargo buscarPorId(Long id) {
        Cargo cargoEncontrado = cargoRepositorio.findById(id)
            .orElseThrow(() -> new CargoNaoEncontradoException(id));

        return cargoEncontrado;
    }

    public Cargo cadastrar(Cargo cargo) {
        return cargoRepositorio.save(cargo);
    }

    public Cargo cadastrar(CargoDto cargoDto) {
        Cargo cargo = cargoMapper.fromDto(cargoDto);

        return cargoRepositorio.save(cargo);
    }

    public Cargo atualizar(Cargo cargo, Long id) {
        buscarPorId(id);

        return cargoRepositorio.save(cargo);
    }

    public Cargo atualizar(CargoDto cargoDto, Long id) {
        Cargo existingCargo = buscarPorId(id);

        Cargo cargo = cargoMapper.fromDto(cargoDto);
        cargo.setId(existingCargo.getId());

        return cargoRepositorio.save(cargo);
    }

    public void excluirPorId(Long id) {
        Cargo cargoEncontrado = buscarPorId(id);

        if (funcionarioRepositorio.findByCargo(cargoEncontrado).isEmpty()) {
            cargoRepositorio.delete(cargoEncontrado);
        } else {
            throw new CargoPossuiFuncionariosException(id);
        }

    }

}

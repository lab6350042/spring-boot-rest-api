package br.com.treinaweb.twprojetos.servicos;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.treinaweb.twprojetos.api.dto.EquipeDto;
import br.com.treinaweb.twprojetos.api.dto.ProjetoDto;
import br.com.treinaweb.twprojetos.api.mapper.ProjetoMapper;
import br.com.treinaweb.twprojetos.entidades.Funcionario;
import br.com.treinaweb.twprojetos.entidades.Projeto;
import br.com.treinaweb.twprojetos.excecoes.ProjetoNaoEncontradoException;
import br.com.treinaweb.twprojetos.repositorios.ProjetoRepositorio;

@Service
public class ProjetoServico {

    @Autowired
    private ProjetoRepositorio projetoRepositorio;

    @Autowired
    private ProjetoMapper projetoMapper;

    @Autowired
    private FuncionarioServico funcionarioServico;

    public List<Projeto> buscarTodos() {
        return projetoRepositorio.findAll();
    }

    public Page<Projeto> buscarTodos(Pageable pageable) {
        return projetoRepositorio.findAll(pageable);
    }

    public Projeto buscarPorId(Long id) {
        return projetoRepositorio.findById(id)
                .orElseThrow(() -> new ProjetoNaoEncontradoException(id));
    }

    public Projeto cadastrar(Projeto projeto) {
        return projetoRepositorio.save(projeto);
    }

    public Projeto cadastrar(ProjetoDto projetoDto) {
        Projeto projeto = projetoMapper.fromDto(projetoDto);

        return projetoRepositorio.save(projeto);
    }

    public Projeto atualizar(Projeto projeto, Long id) {
        buscarPorId(id);

        return projetoRepositorio.save(projeto);
    }

    public List<Funcionario> atualizarEquipe(EquipeDto equipeDto, Long id) {
        Projeto projeto = buscarPorId(id);

        List<Funcionario> equipe = new ArrayList<>();

        equipeDto.getEquipe().forEach(funcionarioId -> {
            Funcionario funcionario = funcionarioServico.buscarPorId(funcionarioId);

            equipe.add(funcionario);
        });

        projeto.setEquipe(equipe);
        projetoRepositorio.save(projeto);

        return equipe;
    }
    
   public Projeto atualizar(ProjetoDto projetoDto, Long id) {
        buscarPorId(id);

        Projeto projeto = projetoMapper.fromDto(projetoDto);
        projeto.setId(id);

        return projetoRepositorio.save(projeto);
    }

    public void excluirPorId(Long id) {
        Projeto projetoEncontrado = buscarPorId(id);

        projetoRepositorio.delete(projetoEncontrado);
    }

}

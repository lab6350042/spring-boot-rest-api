package br.com.treinaweb.twprojetos.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  private static final String EMAIL = "henriquebeckmann2@gmail.com";
  private static final String AUTHOR = "Henrique Beckmann";
  private static final String URL_CONTACT = "henriquebeckmann.com.br";
  private static final String API_VERSION = "1.0.0";
  private static final String API_DESCRIPTION = "API de gerenciamento de projetos";
  private static final String API_TITLE = "Spring Boot Rest API";
  private static final String BASE_PACKAGE = "br.com.treinaweb.twprojetos.api";

  private ApiInfo buildApiInfo() {
    return new ApiInfoBuilder()
        .title(API_TITLE)
        .description(API_DESCRIPTION)
        .version(API_VERSION)
        .contact(new Contact(AUTHOR, URL_CONTACT, EMAIL))
        .build();
  }

  @Bean
  public Docket getDocket() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
        .paths(PathSelectors.any())
        .build().apiInfo(buildApiInfo());
  }
}
